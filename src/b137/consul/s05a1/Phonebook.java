package b137.consul.s05a1;

import java.util.ArrayList;

public class Phonebook{
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    // Constructor
    public Phonebook(){}
    public Phonebook(Contact contact){
        this.contacts.add(contact);
    }

    // Getter
    public ArrayList<Contact> getContacts(){
        return this.contacts;
    }

    // Setter
    public void setContacts(Contact contact){
        this.contacts.add(contact);
    }
}
