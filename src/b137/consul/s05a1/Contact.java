package b137.consul.s05a1;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Contact {
    private String name;
    private ArrayList<String> numbers = new ArrayList<String>();
    private ArrayList<String> addresses = new ArrayList<String>();

    // Constructor
    public Contact(){};

    public Contact (String number, String address) {
        this.numbers.add(number);
        this.addresses.add(address);
    }

    // Getter
    public String getName(){
        return this.name;
    }
    public String getNumbers(){
        return this.numbers.stream().map(Object :: toString).collect(Collectors.joining("\n"));
    }
    public String getAddresses() {
        return addresses.stream().map(Object :: toString).collect(Collectors.joining("\n"));
    }

    // Setter

    public void setName(String name) {
        this.name = name;
    }
    public void setNumbers(String number) {
        this.numbers.add(number);
    }
    public void setAddresses(String address){
        this.addresses.add(address);
    }
}
