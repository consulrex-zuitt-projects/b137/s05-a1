package b137.consul.s05a1;

import java.lang.reflect.Field;

public class Main {

    public static void main(String[] args) {
        System.out.println("Contacts and Phonebooks");

        System.out.println();

        // Instance of Phonebook
        Phonebook phoneBook = new Phonebook();

        // Instance of Contact
        Contact contact1 = new Contact("+639053631246", "Maryland USA");
        Contact contact2 = new Contact("+639155057302", "New York USA");

        // Setting values for Contact1
        contact1.setName("Steve Rogers");
        contact1.setNumbers("+639195461245");
        contact1.setAddresses("Washington DC USA");

        // Setting values for Contact2
        contact2.setName("Tony Stark");
        contact2.setNumbers("+639084357845");
        contact2.setAddresses("Los Angeles USA");

        // Setting values for phoneBook
        phoneBook.setContacts(contact1);
        phoneBook.setContacts(contact2);


        if (phoneBook == null) {
            System.out.println("Phonebook is empty");
        } else {
            for (int i = 0; i < phoneBook.getContacts().size(); i++) {
                System.out.println(phoneBook.getContacts().get(i).getName());
                System.out.println("--------------");
                System.out.println(phoneBook.getContacts().get(i).getName() + " has the following registered numbers:");
                System.out.println(phoneBook.getContacts().get(i).getNumbers());
                System.out.println("----------------------------------");
                System.out.println(phoneBook.getContacts().get(i).getName() + " has the following registered addresses:");
                System.out.println(phoneBook.getContacts().get(i).getAddresses());
                System.out.println("==================================");
            }
        }
    }
}
